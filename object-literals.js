"use strict";

var proto = {
    msg() {
        console.log('this is a message on the prototype');
    }
}

var proto2 = {
    msg() {
        console.log('this is a message on the prototype 2');
    }
}

function handler() {
    console.log('handler function');
}

var obj = {
    // Sets the prototype. "__proto__" or '__proto__' would also work.
    __proto__: proto,

    // Computed property name does not set prototype or trigger early error for duplicate __proto__ properties.
    ['__proto__']: proto2,

    // Shorthand for ‘handler: handler’
    handler,

    // Methods
    toString() {
     // Super calls
     return "sub " + super.toString();
    },

    // Computed (dynamic) property names
    [ "prop_" + (() => 42)() ]: 42
};

console.log(obj.toString.call(1));