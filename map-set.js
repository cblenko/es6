// iterable only - cannot get by key in sets (use an object or map)
// cannot add duplicates - no exception is thrown, they are just ignored (or overwrite the current entry)
var cars = new Set();
cars.add('BMW 3 Series');

for(let car of cars) {
    console.log(car);
}
cars.has('BMW 5 Series');           // false

// iterable and retrievable by key. Key can be any value (primitive or object)
var boats = new Map();
boats.set('SS101', 'Sunseaker Predator 57');

for(let boat of boats) {
    console.log(boat[0], boat[1]);  // 0 - key, 1 - value
}

boats.get('SS101');                 // 'Sunseaker Predator 57'


var o = { value: 1 };
// weaksets can hold objects only. When there is no other reference to that object, it can be garbage collected and removed from the weakset;
var ws = new WeakSet();
ws.add(o);                          // will be added
ws.add( { value: 1 });              // will not be added as there is no further reference to the object;
ws.has(o);                          // true
ws.has({ value: 1 })                // false

// weaksets can have objects only as the key but any value. When there is no other reference to that object, it can be garbage collected and removed from the weakset;
// could be used as provide a history of an object.
var wm = new WeakMap();
wm.set(o, o);
wm.has(o)                           // true
o.value = 2;
wm.has(o)                           // true - we've changed a value on the object but not the object itself

o = { v: 1 };
wm.has(o)                           // false - we've changed a the object
