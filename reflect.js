"use strict";

// have a further read through the documentation

var obj = { name: 'Carl' };
Object.defineProperty(obj, 'street', { value: 'The Street' } );
obj[Symbol('pop')] = 3;

console.log(Reflect.ownKeys(obj));             // ['name', 'street', Symbol(pop)]


function add(a, b){
  this.c = a + b;
}
var instance = Reflect.construct(add, [20, 22]);
console.log(instance.c);                       // 42
