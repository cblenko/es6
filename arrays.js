"use strict";

var a = 1,
    b = 2,
    c = 3;

var arr = Array.of(a, b, c);                // [1, 2, 3]

// [].fill(fillValue, startIndex, endIndex) - NOTE: endIndex doesn't get filled
[0, 0, 0, 0, 0].fill(1, 1, 2);              // [0, 1, 0, 0, 0]

[1, 2, 3].findIndex( (v) => v == 3);        // 2 

var arrI = ["a", "b", "c"].entries()        // returns iterator with key (index) / value pair for each entry
for(let v of arrI) {
    console.log(v);
}

var arrK =["a", "b", "c"].keys()            // returns iterator with keys (index) of the values in the array
for(let v of arrK) {
    console.log(v);
}