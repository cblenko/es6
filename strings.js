"use strict";

// string.repeat
function padF(str, padStr, len) {
    return padStr.repeat(len - str.length) + str;
}

function padB(str, padStr, len) {
    return str + padStr.repeat(len - str.length);
}

// string.includes
function isMyDomain(email) {
    return email.includes('@blenko.co.uk');
}

// normalise
console.log(('\u1E9B\u0323').normalize('NFC'))

// backticks
var multiLineString = `this
is
a
multiline
template
`;

console.log(multiLineString);

// interpolation - NOTE: The interpolcate variable MUST be declared first.
var tObj = { name: 'Carl Blenkinsop' },
    template = `Hello ${tObj.name}`
    
console.log(template);

// tagged templates

function sTag([first, second] = [strings], name, age) {
    return `The first ${first}is called ${name} and is ${age} years old`
}

const name = 'Carl', age = 45
console.log(sTag`person ${name} is ${age}`);


function sTagGen(suffix) {
    return function gen([first, second] = [strings], name, age) {
        return `The first ${first}is called ${name} and is ${age} years old - ${suffix}`
    }
}

console.log(sTagGen('suffix')`person ${name} is ${age}`);