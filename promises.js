"use strict";

let later = new Promise( function(resolve, reject) {
    let result = !!(parseInt((Math.random() * 10), 10) % 2);
    setTimeout(function () {
        console.log(result);
        if(result)
            resolve('1');
        else
            reject();
    }, 1000)
} )

// later.then((v) => {
//     console.log(v);
// }, () => {
//     console.log('error');
// })

// this is a bit more expressive than the above method
later.then((v) => {
    console.log(v);
}).catch( () => {
    console.log('error');
})