// normal function
let add = (a,b) => a + b;
console.log(add(1, 2));

// currying the function
let addf = a => b => a + b;
console.log(addf(1)(2))

// more currying (adder -> fn) -> 1 -> 10
let applyf = fn => a => b => fn(a, b);
let adder = applyf(add);
console.log(adder(1)(10));