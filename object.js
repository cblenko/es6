"use strict";

var o = { 'name': 'Carl' }
var p = { 'address': 'The Street' }
var a = Object.assign({}, o, p);                // { name: 'Carl', address: 'The Street' }
