"use strict";

function* incrementor(initialValue) {
    while(true) {
        yield initialValue++;
    }
}

function userIncrementor() {
    var gen = incrementor(0);

    for(let i = 0 ; i < 10 ; i++) {
        console.log(gen.next());
    }

    // finish the generator as it's infinite in the above incrementor function (while(true))
    gen.return();
}

var numbers = {
    *[Symbol.iterator]({init = 0, max = 10, step = 1} = {}) {
        let currentValue = init;
        for(let i = init; i <= max; i+=step) {
            yield i;
        }
    }
}

for(let num of numbers) {
    console.log(num);
}

let numIterator = numbers[Symbol.iterator]({ init: 0, max: 100 });
for(let num of numIterator) {
    console.log(num);
}

let luckyNumIterator = numbers[Symbol.iterator]({ init: 6, max: 30, step: 4 });
console.log(`My lucky numbers are `, ...luckyNumIterator)

// or shorthanded to
console.log(`My shorthand lucky numbers are `, ...numbers[Symbol.iterator]({ init: 6, max: 30, step: 4 }))



function* tokenizer(val, expr) {
    var result = val.match(expr);

    for(let i = 0 ; i < result.length ; i++) {
        yield result[i];
    }
}

function useTokeniser() {
    var tokenized = tokenizer('carl', /[a-zA-Z]/g),
        token = tokenized.next();

    while(!token.done) {
        console.log(token.value);
        token = tokenized.next();
    }
}

// useTokeniser();
// userIncrementor();
// userBetterIncrementor();