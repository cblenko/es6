"use strict";

// default parameter values
function add(a, b = 0) {
    return a + b;
}
//  console.log(add(10), add(10, 10));

// rest
function sum(...values) {
    return values.reduce( (acc, curr, i, a) => {
        return acc + curr;
    }, 0)
}

//  console.log(sum(1,2,3,4,5,6,7));

// spread - this example isn't very useful but can be use to create a wrapper function (similar to the asPromise method created in node)
function caller(fn, ...args) {
    return fn(...args);
}

console.log(
    caller(add, 10, 10),
    caller(sum, 1,2,3,4,5,6,7)
);