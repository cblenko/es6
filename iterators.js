"use strict";

var iterableObject = {
    a: 1,
    b: 2
}

// this just yeilds 6 values with each next() call - pretty pointless but works
// iterableObject[Symbol.iterator] = function* () {
//     yield 1;
//     yield 2;
//     yield 3;
//     yield 4;
//     yield 5;
//     yield 6;
// }

// iterableObject[Symbol.iterator] = function* () {
//     let keys = Object.keys(this);

//     for(let key in keys) {
//         yield(keys[key]);
//     }
// }

// creates an iteractor over an object (not provided by default)
iterableObject[Symbol.iterator] = function () {
    let keys = Object.keys(this),
        currentIndex = 0;
    return {
        next: () => {
            return currentIndex < keys.length ? {
                value: this[keys[currentIndex++]],
                done: false
            } : {
                value: undefined,
                done: true
            }
        }
    }
}

// for ... of uses the iterator symbol in an object to return the iterator
for(let val of iterableObject) {
    console.log(`for / of: ${val}`);
}

// you can also get all the values using the rest operator
console.log('spread: ', ...iterableObject);


let arr = [1,2,3,4];
let myIterator = arr[Symbol.iterator]();

// uses for / of to iterate through
for(let v of myIterator) {
    console.log(`for / of: ${v}`);
}

// uses temp varaibles and .next() / .done to iterate through values
let myIterator1 = arr[Symbol.iterator]();
let o = myIterator1.next();
while(!o.done) {
    console.log(`.next() and .done: ${o.value}`);
    o = myIterator1.next();
}
