/* proxy - used to "intercept" accessing properties or functions calls. Useful for auditing, logging, profiling etc

  HOOKS for all of the following
  get: ...,                                                   // target.prop = value
  set: ...,                                                   // 'prop' in target
  has: ...,                                                   // delete target.prop
  deleteProperty: ...,                                        // target(...args)
  apply: ...,                                                 // new target(...args)
  construct: ...,                                             // Object.getOwnPropertyDescriptor(target, 'prop')
  getOwnPropertyDescriptor: ...,                              // Object.defineProperty(target, 'prop', descriptor)
  defineProperty: ...,                                        // Object.getPrototypeOf(target), Reflect.getPrototypeOf(target),
  getPrototypeOf: ...,                                        // target.__proto__, object.isPrototypeOf(target), object instanceof target
  setPrototypeOf: ...,                                        // Object.setPrototypeOf(target), Reflect.setPrototypeOf(target)
  enumerate: ...,                                             // for (let i in target) {}
  ownKeys: ...,                                               // Object.keys(target)
  preventExtensions: ...,                                     // Object.preventExtensions(target)
  isExtensible :...                                           // Object.isExtensible(target)
*/


/* for properties
   potential issue with deep objects - you need to proxy each object within
   the object, but then if you want to get an object (see below) you actually end up with a proxy of the object.

   var o = { name: 'Carl', address: { street: 'First Road } }

   var add = o.address      // this would return a proxy of the object (assuming )

*/



// this works for simple shallow objects. For deeper objects the "proxification" would need to be in a function

var obj = {
    name: 'CARL'
}

var handler = {
    get: function(target, property) {
        console.log(`Getting ${property}`)
        return target[property];
    },
    set: function(target, property, value) {
        console.log(`Setting ${property} to ${value}`)
        return (target[property] = value);
    }
}

var objProxy = new Proxy(obj, handler);

objProxy.name;                          // 'CARL'
objProxy.name = "Test"                  // 'Test'


// for functions
var target = function (number) { 
    return number;
};

var handler = {
    apply: function (target, thisArg, args) {
        console.log('proxied function', thisArg);
        return target.call(thisArg, ...args);
    }
};

var p = new Proxy(target, handler);
console.log(p(45))