// import specific exports from a given file - names must match
import { CanalBoat, SpeedBoat, PowerBoat as PB } from './boats.js';

// import to a local namespace
import * as Boats from './boats.js';

// single import
import { Car } from './car.js';

// default import
import def from './default.js';


Boats.CanalBoat();

def();
CanalBoat();
PB();