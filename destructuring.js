"use strict";

// destructuring array
{
	let [user, domain] = ('carl@blenko.co.uk').split('@');
	console.log(`User: ${user}, Domain: ${domain}`);
}


// destructuring object
{
	var name,
	    email;

	({ name, email } = { name: 'Carl Blenkinsop', email: 'carl@blenko.co.uk' });
}

console.log(name, email)

// destructuring object - simpler
{
	let { name, email} = { name: 'Carl Blenkinsop', email: 'carl@blenko.co.uk' }
	console.log(name, email);
}

// nested object destructuring
{
	let obj = { name: 'Carl Blenkinsop', email: 'carl@blenko.co.uk', address: { street: '69 Bracken Road', town: 'Brighouse'}}
	let { address: { street }} = obj;
	console.log(`nested object destructuring: ${street}`);
}

// nested object destructuring with rename
{
	let obj = { name: 'Carl Blenkinsop', email: 'carl@blenko.co.uk', address: { street: '69 Bracken Road', town: 'Brighouse'}}
	let { address: { street: myStreet = 'Not Found' }} = obj;
	console.log(`nested object destructuring with rename: ${myStreet}`);
}

// nested object destructuring with rename and default value
{
	let obj = { name: 'Carl Blenkinsop', email: 'carl@blenko.co.uk', address: { street: '69 Bracken Road', town: 'Brighouse'}}
	let { address: { street: myStreet = 'Not Found' }} = obj;
	console.log(`nested object destructuring with rename and default value: ${myStreet}`);
}