"use strict";

class Animal {
    constructor(legs) {
        // used by getter / setter - not actually private
        this._legs = legs;

        // public property
        this.legCount = legs
    }

    get legs() { return this._legs + ' legs'; }
    set legs(v) { this._legs = v; }
    
}

var a = new Animal(4);
console.log(a.legs, a.legCount);
