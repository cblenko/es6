"use strict";

// arrow functions
var evens = [0,2,4,6,8,10],
    odds = evens.map( (v) => v + 1 ),               // implicit return and expression body
    odds1 = evens.map( (v) => { return v + 1 } )    // explicit return and statement body


// lexical this
var person = {
    _name: 'Carl Blenkinsop',
    _friends: ['Chris', 'Nick', 'Steve'],
    printFriendsExpr: function() {                  // NOTE: We cannot use an arrow function here hence the shorthand function definition
        this._friends.forEach( (f) => {
            console.log(`${this._name} knows ${f}`);
        })
    },
    printFriendsStmt() {
        this._friends.forEach( (f) => console.log(`${this._name} knows ${f}`) )
    }
}

console.log(person.printFriendsExpr())
console.log(person.printFriendsStmt())


// Lexical arguments

/*      Arrows are a function shorthand using the => syntax. 
        They support both expression and statement bodies. Unlike 
        functions, arrows share the same lexical this as their 
        surrounding code. If an arrow is inside another function, it
        shares the “arguments” variable of its parent function.
*/

function square() {
    let example = (a) => {
      let numbers = [];
      for (let number of arguments) {
        numbers.push(number * number);
      }
  
      return numbers;
    };
  
    return example(1);
}

// example of passing additional arguments into returned inner functions have to impact on the arguments collection
function argF() {
    console.log('outer', arguments)
    return () => {
        console.log('mid', arguments)
        return () => {
            console.log('inner', arguments)
            return arguments;
        }
    }
}
 console.log(argF(5)(4)(3))

// arrow function with arguments
var addFn = (a, b ,c) => {
    console.log(a, b, c);
}

addFn(1, 2, 3)