"use strict";

// nothing much to show here - they are pretty simple concepts

function isAdmin(user) {
    const ADMIN_ROLE = 'ADMIN'

    // this will throw if uncommented for obvious reasons
    // ADMIN_ROLE = 1

    return user.role === ADMIN_ROLE;
}

console.log(isAdmin({ role: 'ADMIN' }));

// let is block scoped - unlike var which is function (or global if declared outside of a function) scoped
function checkLet() {
    let x = 0;

    {
        const x = 1;
        console.log(x);
    }

    console.log(x);
}

checkLet();